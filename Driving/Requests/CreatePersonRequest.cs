﻿using Driving.Models.Enums;
using System;

namespace Driving.Requests
{
    public class CreatePersonRequest
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public Role Role { get; set; }

        public Guid? VehicleId { get; set; }
    }
}
