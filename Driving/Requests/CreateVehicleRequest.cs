﻿using Driving.Models;
using System.Collections.Generic;

namespace Driving.Requests
{
    public class CreateVehicleRequest
    {
        public string Name { get; set; }

        public List<LogRequest> Logs { get; set; }
    }
}
