﻿using System;

namespace Driving.Requests
{
    public class LogRequest
    {
        public DateTime CreationDate { get; set; }

        public int Speed { get; set; }

        public string Position { get; set; }

        public Guid DriverId { get; set; }
    }
}
