﻿using System;

namespace Driving.Requests
{
    public class ReportLogRequest
    {
        public Guid DriverId{ get; set; }

        public string Position { get; set; }

        public int Speed { get; set; }
    }
}
