﻿using Driving.Models;
using Driving.Requests;
using System;
using System.Collections.Generic;

namespace Driving.Services.Interfaces
{
    public interface IVehicleService
    {
        public IEnumerable<Vehicle> Get(DateTime? date, Guid? vehicleId);

        public Vehicle GetById(Guid id);

        public Vehicle Create(Vehicle vehicle);

        public void Delete(Guid id);

        public Vehicle AssignDriver(Guid vehicleId, Guid driverId);

        public IEnumerable<DriverVehiclesHistoryDto> GetVehicleHistory(Guid driverId);

        public Vehicle Report(ReportLogRequest reportLogRequest, Guid vehicleId);

        public IEnumerable<Vehicle> GetDangerLogs(int maxSpeed);
    }
}
