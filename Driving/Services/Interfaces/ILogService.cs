﻿using Driving.Models;
using Driving.Requests;
using System.Collections.Generic;

namespace Driving.Services.Interfaces
{
    public interface ILogService
    {
        List<Log> GenerateLogs(List<LogRequest> logs);
    }
}
