﻿using Driving.Models;
using System;
using System.Collections.Generic;

namespace Driving.Services.Interfaces
{
    public interface IPersonService
    {
        public IEnumerable<Person> GetAll();

        public Person GetById(Guid id);

        public Person Create(Person person);

        public void Delete(Guid id);

        public bool AssignVehicle(Guid personId, Guid vehicleId);
    }
}
