﻿using Driving.DataAccess;
using Driving.DataAccess.Interfaces;
using Driving.Models;
using Driving.Services.Interfaces;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace Driving.Services
{
    public class PersonService : IPersonService
    {
        private readonly IMongoCollection<Person> _persons;

        public PersonService(IMongoDatabase database)
        {
            _persons = database.GetCollection<Person>(MongoCollections.PERSON);
        }

        public IEnumerable<Person> GetAll()
        {
            return _persons.Find( persons => true).ToEnumerable();
        }

        public Person GetById(Guid id)
        {
            return _persons.Find(p => p.Id == id).FirstOrDefault();
        }

        public Person Create(Person person)
        {
            _persons.InsertOne(person);
            return person;
        }

        public void Delete(Guid id)
        {
            _persons.DeleteOne(person => person.Id == id);
        }

        public bool AssignVehicle(Guid personId, Guid vehicleId)
        {
            var filter = Builders<Person>.Filter.Eq(p => p.Id, personId);
            var update = Builders<Person>.Update.Set(p => p.VehicleId, vehicleId);

            var result = _persons.UpdateOneAsync(filter, update);

            return result.Result.ModifiedCount == 1;
        }
    }
}
