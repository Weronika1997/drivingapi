﻿using Driving.DataAccess;
using Driving.Models;
using Driving.Requests;
using Driving.Services.Interfaces;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Driving.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly IMongoCollection<Vehicle> _vehicles;

        public VehicleService(IMongoDatabase database)
        {
            _vehicles = database.GetCollection<Vehicle>(MongoCollections.VEHICLE);
        }

        public IEnumerable<Vehicle> Get(DateTime? date, Guid? vehicleId)
        {
            if (!date.HasValue || vehicleId == null)
                return _vehicles.Find(vehicle => true).ToEnumerable();
            else
                return _vehicles.Find(vehicle => vehicle.Id == vehicleId).ToEnumerable()
                            .Select(x => new Vehicle { Id = x.Id, Name = x.Name, Logs = x.Logs
                                        .Where(l => l.CreationDate.Date == date.Value.Date).ToList() });

        }

        public Vehicle GetById(Guid id)
        {
            return _vehicles.Find(p => p.Id == id).FirstOrDefault();
        }

        public Vehicle Create(Vehicle vehicle)
        {
            _vehicles.InsertOne(vehicle);
            return vehicle;
        }

        public void Delete(Guid id)
        {
            _vehicles.DeleteOne(vehicle => vehicle.Id == id);
        }

        public Vehicle AssignDriver(Guid vehicleId, Guid driverId)
        {
            var vehicle = _vehicles.Find(p => p.Id == vehicleId).FirstOrDefault();
            if (vehicle == null)
                return null;

            vehicle.Logs.Add(new Log 
            { 
                Id = Guid.NewGuid(),
                CreationDate = DateTime.Now.ToUniversalTime(),
                DriverId = driverId
            });

            _vehicles.ReplaceOne(x => x.Id == vehicleId, vehicle);
            return vehicle ;
        }

        public IEnumerable<DriverVehiclesHistoryDto> GetVehicleHistory(Guid driverId)
        {
            var historicalAssigment = _vehicles.Find(vehicle => true).ToEnumerable()
                        .Where(x => x.Logs.Where(l => l.DriverId == driverId && string.IsNullOrEmpty(l.Position)).Any())
                        .Select(x => new DriverVehiclesHistoryDto { VehicleName = x.Name, CreationDate = x.Logs
                            .Where(l => l.DriverId == driverId && string.IsNullOrEmpty(l.Position))
                                .Select(l => l. CreationDate).FirstOrDefault()});

            return historicalAssigment;
        }

        public Vehicle Report(ReportLogRequest reportLogRequest, Guid vehicleId)
        {
            var vehicle = _vehicles.Find(p => p.Id == vehicleId).FirstOrDefault();
            if (vehicle == null)
                return null;

            vehicle.Logs.Add(new Log 
            {
                Id = Guid.NewGuid(),
                CreationDate  = DateTime.Now.ToUniversalTime(),
                Position = reportLogRequest.Position,
                Speed = reportLogRequest.Speed,
                DriverId = reportLogRequest.DriverId
            });
           _vehicles.ReplaceOne(x => x.Id == vehicleId, vehicle);
            
            return vehicle;
        }

        public IEnumerable<Vehicle> GetDangerLogs(int maxSpeed)
        {
            var dangerLogs = _vehicles.Find(vehicle => true).ToEnumerable()
                        .Where(x => x.Logs.Where(l => l.Speed >= maxSpeed).Any())
                        .Select(x => new Vehicle { Id = x.Id, Name = x.Name, Logs = x.Logs
                            .Where(l => l.Speed >= maxSpeed).ToList() });
            return dangerLogs;
        }
    }
}
