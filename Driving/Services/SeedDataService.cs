﻿using Bogus;
using Driving.DataAccess.Interfaces;
using Driving.Models;
using Driving.Models.Enums;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace Driving.Services
{
    public static class SeedDataService
    {
        public static void SeedDatabase(IDrivingDatabaseSettings settings)
        {
            IMongoCollection<Vehicle> _vehicles;
            IMongoCollection<Driving.Models.Person> _persons;

            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _vehicles = database.GetCollection<Vehicle>(settings.VehicleCollectionName);
            _persons = database.GetCollection<Driving.Models.Person>(settings.PersonCollectionName);

            var faker = new Faker();
            var persons =  CreatePersons(faker);
            var vehicles = CreateVehicles(faker, persons);
            
            _persons.InsertMany(persons);
            _vehicles.InsertMany(vehicles);
        }

        private static List<Driving.Models.Person> CreatePersons(Faker faker)
        {
            var persons = new List<Driving.Models.Person>();
            for (int i = 0; i < 10; i++)
            {
                persons.Add(new Driving.Models.Person
                {
                    Id = Guid.NewGuid(),
                    Firstname = faker.Name.FirstName(),
                    Lastname = faker.Name.LastName(),
                    Role = i >= 7 ? Role.Supervisor.ToString() : Role.Driver.ToString(),
                    VehicleId = null
                });
            }
            return persons;
        }

        private static List<Vehicle> CreateVehicles(Faker faker, List<Driving.Models.Person> persons)
        {
            var vehicles = new List<Vehicle>();
            for (int i = 0; i < 7; i++)
            {
                var logs = new List<Log>();
                for (int x = 0; x < 2; x++)
                {
                    logs.Add(new Log
                    {
                        Id = Guid.NewGuid(),
                        CreationDate = faker.Date.Recent(),
                        Position = faker.Address.County() + " " + faker.Address.City(),
                        Speed = faker.Random.Int(0, 100),
                        DriverId = persons[faker.Random.Int(0, persons.Count - 1)].Id
                    });
                }

                vehicles.Add(new Vehicle
                {
                    Id = Guid.NewGuid(),
                    Name = string.Join(" ", faker.Vehicle.Manufacturer(), faker.Vehicle.Model()),
                    Logs = logs
                });
            }
            return vehicles;
        }
    }
}
