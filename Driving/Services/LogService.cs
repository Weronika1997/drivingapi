﻿using Driving.Models;
using Driving.Requests;
using Driving.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace Driving.Services
{
    public class LogService : ILogService
    {
        public List<Log> GenerateLogs(List<LogRequest> logs)
        {
            var vehicleLogs = new List<Log>();
            foreach (var log in logs)
            {
                vehicleLogs.Add(new Log 
                {
                    Id = Guid.NewGuid(),
                    CreationDate = log.CreationDate,
                    Position = log.Position,
                    Speed = log.Speed,
                    DriverId = log.DriverId
                });
            }
            return vehicleLogs;
        }
    }
}
