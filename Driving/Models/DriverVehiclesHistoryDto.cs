﻿using System;

namespace Driving.Models
{
    public class DriverVehiclesHistoryDto
    {
        public string VehicleName { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
