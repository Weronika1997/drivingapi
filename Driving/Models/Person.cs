﻿using Driving.Models.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Driving.Models
{
    public class Person
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string InternalId { get; set; }

        [BsonElement("Id")]
        public Guid Id { get; set; }

        [BsonElement("Fistname")]
        public string Firstname { get; set; }

        [BsonElement("Lastname")]
        public string Lastname { get; set; }

        [BsonElement("Role")]
        public string Role { get; set; }

        [BsonElement("VehicleId")]
        public Guid? VehicleId { get; set; }
    }
}
