﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Driving.Models
{
    public class Log
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string InternalId { get; set; }

        public Guid Id { get; set; }

        public DateTime CreationDate { get; set; }

        public int Speed { get; set; }

        public string Position { get; set; }

        public Guid DriverId { get; set; }
    }
}
