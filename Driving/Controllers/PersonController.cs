﻿using Driving.Models;
using Driving.Requests;
using Driving.Services;
using Driving.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Driving.Controllers
{
    [Route("api/persons")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly IPersonService _personService;
        private readonly IVehicleService _vehicleService;

        public PersonController
            (IPersonService personService,
             IVehicleService vehicleService)
        {
            _personService = personService;
            _vehicleService = vehicleService;
        }

        [HttpGet]
        public ActionResult<List<Person>> GetAll()
        {
            return Ok(_personService.GetAll());
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<List<Person>> GetById(Guid id)
        {
            var person = _personService.GetById(id);
            if (person == null)
                return NotFound();

            return Ok(person);
        }

        [HttpPost]
        public IActionResult Post([FromBody] CreatePersonRequest createPersonRequest)
        {
            var person = new Person
            {
                Id = Guid.NewGuid(),
                Firstname = createPersonRequest.Firstname,
                Lastname = createPersonRequest.Lastname,
                Role = createPersonRequest.Role.ToString(),
                VehicleId = createPersonRequest.VehicleId
            };
            _personService.Create(person);

            return Ok(person);
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(Guid id)
        {
            if (_personService.GetById(id) == null)
                return NotFound();
            
            _personService.Delete(id);
            return Ok($"Successfully deleted person with id : {id}");
        }

        [HttpGet]
        [Route("{id}/vehicles")]
        public IActionResult GetDrivenVehicles(Guid id)
        {
            if (_personService.GetById(id) == null)
                return NotFound();
           
            return Ok(_vehicleService.GetVehicleHistory(id));
        }

    }
}