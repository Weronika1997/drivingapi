﻿using Driving.Models;
using Driving.Requests;
using Driving.Services;
using Driving.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Driving.Controllers
{
    [Route("api/vehicle")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly IVehicleService _vehicleService;
        private readonly ILogService _logService;
        private readonly IPersonService _personService;

        public VehicleController(
            IVehicleService vehicleService, 
            ILogService logService,
            IPersonService personService)
        {
            _vehicleService = vehicleService;
            _logService = logService;
            _personService = personService;
        }

        [HttpGet]
        public ActionResult<List<Vehicle>> Get([FromQuery] DateTime? date, Guid? vehicleId)
        {
            return Ok(_vehicleService.Get(date, vehicleId));
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<List<Vehicle>> Get(Guid id)
        {
            var vehicle = _vehicleService.GetById(id);
            if (vehicle == null)
                return NotFound();

            return Ok(vehicle);
        }

        [HttpGet]
        [Route("dangers")]
        public ActionResult<List<Vehicle>> Get([FromQuery] int maxSpeed)
        {
            return Ok(_vehicleService.GetDangerLogs(maxSpeed));
        }

        [HttpPost]
        public IActionResult Post([FromBody] CreateVehicleRequest createVehicleRequest)
        {
            var vehicle = new Vehicle
            {
                Id = Guid.NewGuid(),
                Name = createVehicleRequest.Name,
                Logs = _logService.GenerateLogs(createVehicleRequest.Logs)

            };
            _vehicleService.Create(vehicle);
            return Ok(vehicle);
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(Guid id)
        {
            _vehicleService.Delete(id);
            return Ok();
        }

        [HttpPost]
        [Route("{vehicleId}/assign-driver/{driverId}")]
        public IActionResult AssignDriver(Guid vehicleId, Guid driverId)
        {
            if (_vehicleService.AssignDriver(vehicleId, driverId) == null)
                return NotFound(new { mesage = $"Vehicle with aforementioned id {vehicleId} does not exists" });

            if (!_personService.AssignVehicle(driverId, vehicleId))
                return NotFound(new { mesage = $"Driver with aforementioned id {vehicleId} does not exists" });

            return Ok(new { message = "Driver assigned to vehicle with success!"});
        }

        [HttpPost]
        [Route("{vehicleId}/report")]
        public IActionResult Report([FromRoute] Guid vehicleId, [FromBody] ReportLogRequest reportLogRequest)
        {
            if (_vehicleService.Report(reportLogRequest, vehicleId) == null)
                return NotFound();
            
            return Ok();
        }



    }
}