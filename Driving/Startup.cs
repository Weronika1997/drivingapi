using Driving.DataAccess;
using Driving.DataAccess.Interfaces;
using Driving.Services;
using Driving.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Driving
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<DrivingDatabaseSettings>(
                Configuration.GetSection(nameof(DrivingDatabaseSettings)));

            services.AddSingleton<IDrivingDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<DrivingDatabaseSettings>>().Value);

            services.AddSingleton<IMongoClient, MongoClient>
                (sp => new MongoClient(Configuration["DrivingDatabaseSettings:ConnectionString"]));

            services.AddScoped(sp =>
            {
                var client = sp.GetRequiredService<IMongoClient>();
                return client.GetDatabase(Configuration["DrivingDatabaseSettings:DatabaseName"]);
            });

            services.AddScoped<IPersonService, PersonService>();
            services.AddScoped<IVehicleService, VehicleService>();
            services.AddScoped<ILogService, LogService>();

            services.AddControllers();

            services.AddSwaggerGen();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "DrivingAPI");
            });

            //uncomment while dealing the first with database
           // SeedDataService.SeedDatabase(app.ApplicationServices.GetService<IDrivingDatabaseSettings>());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
