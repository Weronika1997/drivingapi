﻿namespace Driving.DataAccess.Interfaces
{
    public interface IDrivingDatabaseSettings
    {
        public string PersonCollectionName { get; set; }

        public string VehicleCollectionName { get; set; }

        public string ConnectionString { get; set; }

        public string DatabaseName { get; set; }
    }
}
