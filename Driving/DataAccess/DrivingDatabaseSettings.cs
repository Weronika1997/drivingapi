﻿using Driving.DataAccess.Interfaces;

namespace Driving.DataAccess
{
    public class DrivingDatabaseSettings : IDrivingDatabaseSettings

    {
        public string PersonCollectionName { get; set; }

        public string VehicleCollectionName { get; set; }

        public string ConnectionString { get; set ; }

        public string DatabaseName { get; set; }
       
    }
}
