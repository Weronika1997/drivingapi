﻿namespace Driving.DataAccess
{
    public static class MongoCollections
    {
        public const string VEHICLE = "Vehicle";

        public const string PERSON = "Person";
    }
}
